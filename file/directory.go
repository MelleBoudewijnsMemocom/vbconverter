package file

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"strings"

	"bitbucket.com/barthr/vbconverter/work"
)

// FileCompleter holds a matched file to be used in the Worker Pool
type fileCompleter struct {
	path     string
	file     os.FileInfo
	srcDir   string
	outDir   string
	fileName string
}

// Task is a method to satisfy the Worker interface
// the Method is to be executed in the worker Pool
func (f fileCompleter) Task() {
	//Send to Server
	resultFile, err := ioutil.ReadFile(f.path)
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	f.createJsExtension()
	//TODO
	ioutil.WriteFile(f.outDir+"/"+f.fileName, resultFile, os.ModePerm)
}

// createJsExtension splits the fileName on "."
// There will be an array created with 2 items
// The first item is the filename and the second item is the extension
// the second item get's modified to .js (javascript extension name)
// the fileName variable of the fileCompleter struct will be set to this
// Example: text.txt will become text.js
func (f *fileCompleter) createJsExtension() {
	if len(f.fileName) == 0 {
		return
	}
	items := strings.Split(f.fileName, ".")
	f.fileName = fmt.Sprintf(items[0] + ".js")
	fmt.Println(f.fileName)
}

// StageFiles sends all the files mathing a certain pattern to the worker Pool
// StageFiles should be passed into an filepath.Walk function
// *work.Pool is a pointer to the unbuffered worker pool
// StageFiles returns a walking function
func StageFiles(pool *work.Pool, source, output string) filepath.WalkFunc {
	return func(fp string, fi os.FileInfo, err error) error {
		if err != nil {
			fmt.Println(err) // can't walk here,
			return nil       // but continue walking elsewhere
		}
		if fi.IsDir() {
			return nil // not a file.  ignore.
		}
		matched, err := filepath.Match("*.txt", fi.Name())
		if err != nil {
			fmt.Println(err) // malformed pattern
			return err       // this is fatal.
		}
		if matched {
			fc := fileCompleter{
				path:     fp,
				file:     fi,
				srcDir:   source,
				outDir:   output,
				fileName: fi.Name(),
			}
			pool.Run(&fc)
		}
		return nil
	}
}
