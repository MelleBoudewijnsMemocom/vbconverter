package work

import "sync"

// Worker must be implemented to use the work Pool
type Worker interface {
	Task()
}

// Pool provdes a pool of goroutines that can execute a Worker task
type Pool struct {
	work chan Worker
	wg   sync.WaitGroup
}

// New is a Factory method for creating a pool of workers
func New(maxGoroutines int) *Pool {
	p := Pool{
		work: make(chan Worker),
	}

	p.wg.Add(maxGoroutines)

	for i := 0; i < maxGoroutines; i++ {
		go func() {
			for w := range p.work {
				w.Task()
			}
			p.wg.Done()
		}()
	}
	return &p
}

// Run submits work to the pool.
func (p *Pool) Run(w Worker) {
	p.work <- w
}

// Shutdown waits for all the goroutines to shutdown.
func (p *Pool) Shutdown() {
	close(p.work)
	p.wg.Wait()
}
