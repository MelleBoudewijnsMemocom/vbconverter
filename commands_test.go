package main

import "testing"

func TestDirCreator(t *testing.T) {
	t.Log("Testing Directory creating function")
	testPath := []string{".", "&34"}

	shouldExist := createDir(testPath[0])
	shouldNotExist := createDir(testPath[1])

	if shouldExist != testPath[0] {
		t.Errorf("%s is no the same as %s", shouldExist, testPath[0])
	}

	if shouldNotExist == testPath[1] {
		t.Errorf("%s is not the same as %s", shouldNotExist, testPath[1])
	}
}
