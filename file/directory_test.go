package file

import "testing"

func TestJsExtension(t *testing.T) {
	t.Log("Testing the Javascript extension creator function")
	mockFileCompleter := new(fileCompleter)
	mockFileCompleter.fileName = "test.txt"

	expectedOutcome := "test.js"
	mockFileCompleter.createJsExtension()

	if mockFileCompleter.fileName != expectedOutcome {
		t.Errorf("%s does not equals %s", mockFileCompleter.fileName, expectedOutcome)
	}
}
