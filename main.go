package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"bitbucket.com/barthr/vbconverter/file"
	"bitbucket.com/barthr/vbconverter/work"
)

var (
	srcDir string
	outDir string
)

const (
	testDir = "/home/bart/.gvm/pkgsets/go1.6/global/src/bitbucket.com/barthr/vbconverter/testdir"
)

//usage prints the instructions of the cli
func usage() {
	fmt.Fprintf(os.Stderr, "\nNAME:\n  Vbc - Vbscript to Javascript CLI\n")
	fmt.Fprintf(os.Stderr, "\nUSAGE:\n  Vbc -s [dir] -o [dir]\n")
	fmt.Fprintf(os.Stderr, "\nFLAGS:\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func main() {
	flag.StringVar(&srcDir, "s", "", "Set the source directory\n")
	flag.StringVar(&outDir, "o", "", "Set the output directory")
	flag.Usage = usage

	flag.Parse()

	if flag.NArg() > 0 {
		usage()
		os.Exit(0)

	} else {
		if flag.NFlag() >= 1 {
			if ex, err := exists(srcDir); ex {
				if err != nil {
					fmt.Println(err)
					os.Exit(2)
				}
				outDir = createDir(outDir)
				fmt.Printf("This is the srcDir: %s \nThis is the outDir: %s\n", srcDir, outDir)
				fileProcessor()
				os.Exit(0)
			}
			fmt.Printf("Cannot find directory on given path: %s\n", srcDir)
			os.Exit(2)
		}
		usage()
		os.Exit(2)
	}
}

// Todo
func createDir(dir string) string {
	outDirExists, err := exists(dir)

	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	if !outDirExists { //Create target folder in Current directory
		err = os.Mkdir("./target", 0700)
		dir, err = os.Getwd()
		dir += `/target`
	}

	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}

	return dir
}

// exists checks if the given path exists on the system
// if os.IsNotExists error occurs the dir doesn't exists
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// fileProcessor instantiate a worker pool with 1 goroutine
// waits for the file to process
// and shutdown the worker pool
// by making use of the worker pool we can use unbuffered channels to synchronize the file converting
// this will ensure that every file is converted
func fileProcessor() {
	pool := work.New(1) // Create Pool with 1 Goroutines

	filepath.Walk(srcDir, file.StageFiles(pool, srcDir, outDir)) // Walk on the testDir (50000 files)

	defer pool.Shutdown() // Shut down the pool and wait for all the Goroutines to finish
}
